import Vue from 'vue'
import App from './App.vue'

import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import {library} from '@fortawesome/fontawesome-svg-core'
import {faSearch, faRss, faBars} from '@fortawesome/free-solid-svg-icons'
import {faLinkedinIn, faTwitter, faPinterestP, faGooglePlusG} from '@fortawesome/free-brands-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'

import {
    ValidationObserver,
    ValidationProvider,
    extend,
    localize
} from "vee-validate";
import en from "vee-validate/dist/locale/en.json";
import * as rules from "vee-validate/dist/rules";

import router from "./router";

import '@/assets/css/style.css'
import '@/assets/css/header.css'
import './registerServiceWorker'

library.add(faSearch, faLinkedinIn, faTwitter, faPinterestP, faGooglePlusG, faRss, faBars)

Vue.config.productionTip = false

Vue.use(BootstrapVue)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.component("ValidationObserver", ValidationObserver);
Vue.component("ValidationProvider", ValidationProvider);

Object.keys(rules).forEach(rule => {
    extend(rule, rules[rule]);
});

localize("en", en);


new Vue({
    router,
    render: h => h(App),
}).$mount('#app')