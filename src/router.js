import Vue from 'vue'

import Router from 'vue-router'

import IndexPage from './pages/Index'
import StyleDemo from './pages/Style-demo'
import FullWidth from './pages/Full-width'
import Dropdown from './pages/Dropdown'
import Gallery from './pages/Gallery'
import Portfolio from './pages/Portfolio'
import LinkText from './pages/Link-text'
import LongLinkText from './pages/Long-link-text'
import ErrorPage from './pages/Error-page'

Vue.use(Router)

export default new Router ({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: IndexPage,
            name: 'index'
        },
        {
            path: '/style-demo',
            component: StyleDemo,
            name: 'style-demo'
        },
        {
            path: '/full-width',
            component: FullWidth,
            name: 'full-width'
        },
        {
            path: '/dropdown',
            component: Dropdown,
            name: 'dropdown'
        },
        {
            path: '/gallery',
            component: Gallery,
            name: 'gallery'
        },
        {
            path: '/portfolio',
            component: Portfolio,
            name: 'portfolio'
        },
        {
            path: '/linkText',
            component: LinkText,
            name: 'linkText'
        },
        {
            path: '/long-link-text',
            component: LongLinkText,
            name: 'long-link-text'
        },
        {
            path: '*',
            component: ErrorPage,
            name: 'error-page'
        }
    ]
})

